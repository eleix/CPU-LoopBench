// C++ Loop Benchmark
// Written by Francis Booth (C) 2017
//

#include<iostream>
#include<iomanip>
#include<fstream>
#include<string>
#include<pthread.h>
#include<stdlib.h>


using namespace std;

//PROTOTYPES
void *DoLoop(void *threadid);
void CountLoop(int NUMTHREADS, int NUMLOOPS);

int main()
{
  bool cont = false;
  char choice;
  int NUMTHREADS;
  ifstream din;
  ofstream dout;
  dout.open("NLC.DAT");


  cout << "------------DISCLAIMER------------" << endl;
  cout << endl;
  cout << "This program goes through a loop for 1 whole minute and at the end outputs how many loops it was able to complete in that timeframe." << endl;
  cout << "This program should not be run on production machines and should only be used to test performance on a system by authorized administrators." << endl;
  cout << "ANY UNAUTHORIZED USE OF THIS PROGRAM TO INTENTIONALLY ISSUE A DENIAL OF SERVICE IS PROHIBITED. PLEASE DO NOT ABUSE THIS PROGRAM." << endl;
  cout << endl;

  do
  {

    cout << "Do you accept the risks and wish to run this program? (Y/N)" << endl;
    cin >> choice;

    switch(choice){
      case 'y' :
      case 'Y' : cont = true; break;
      case 'n' :
      case 'N' : return 0; break; 
      default : break;
    }
  }
  while(!cont);

  cout << "Please specify the number of cores your PC has" << endl;
  cin >> NUMTHREADS;
  NUMTHREADS = NUMTHREADS + 1;

   pthread_t threads[NUMTHREADS];
   int rc;
   int i;
	
   for( i=0; i < NUMTHREADS; i++ ){
      cout << "SYSTEM : Creating Thread, " << i << endl;
      rc = pthread_create(&threads[i], NULL, DoLoop, (void *)i);
		
      if (rc){
         cout << "Error:Unable to create thread" << rc << endl;
         exit(-1);
      }
   }
   	
   pthread_exit(NULL);
   return 0;
}

void CountLoop(int NUMTHREADS, int NUMLOOPS)
{

  float average = 0.0;
  int sum;
  
  average = NUMLOOPS / NUMTHREADS;
  sum = NUMLOOPS;

  NUMTHREADS = NUMTHREADS - 1;
  cout << "This system was able to execute " << sum << " loops within 60 seconds and utilized " << NUMTHREADS << " cores." << endl;
  cout << "Individual core score: " << average << endl;

}
void *DoLoop(void *threadid)
{
    long tid;
    int i;
    string input = "NLX";
    int NUMTHREADS = 0;
    tid = (long)threadid;
    time_t end = time(NULL) + 10;
    long long int NUMLOOPS = 0;
    long int NL0=0;
    long int NL1=0;
    long int NL2=0;
    long int NL3=0;
    long int NL4=0;
    long int NL5=0;
    long int NL6=0;
    long int NL7=0;
    long int NL8=0;
    long int NL9=0;
    long int NL10=0;
    long int NL11=0;
    long int NL12=0;
    long int NL13=0;
	long int NL14=0;
	long int NL15=0;
	long int NL16=0;
	long int NL17=0;
	long int NL18=0;
	long int NL19=0;
	long int NL20=0;
	  ifstream din;
	  ofstream dout("NLC.DAT", std::ios::app);


    //cout << tid << endl;
    switch(tid)
    {
      case 0 : while(time(NULL) <= end) { NL0++; }; break;
      case 1 : while(time(NULL) <= end) { NL1++; }; dout << NL1; dout << "\n"; pthread_exit(NULL);
      case 2 : while(time(NULL) <= end) { NL2++; }; dout << NL2; dout << "\n"; pthread_exit(NULL);
      case 3 : while(time(NULL) <= end) { NL3++; }; dout << NL3; dout << "\n"; pthread_exit(NULL);
      case 4 : while(time(NULL) <= end) { NL4++; }; dout << NL4; dout << "\n"; pthread_exit(NULL);
      case 5 : while(time(NULL) <= end) { NL5++; }; dout << NL5; dout << "\n"; pthread_exit(NULL);
      case 6 : while(time(NULL) <= end) { NL6++; }; dout << NL6; dout << "\n"; pthread_exit(NULL);
      case 7 : while(time(NULL) <= end) { NL7++; }; dout << NL7; dout << "\n"; pthread_exit(NULL);
      case 8 : while(time(NULL) <= end) { NL8++; }; dout << NL8; dout << "\n"; pthread_exit(NULL);
      case 9 : while(time(NULL) <= end) { NL9++; }; dout << NL9; dout << "\n"; pthread_exit(NULL);
      case 10 : while(time(NULL) <= end) { NL10++; }; dout << NL10; dout << "\n"; pthread_exit(NULL);
      case 11 : while(time(NULL) <= end) { NL11++; }; dout << NL11; dout << "\n"; pthread_exit(NULL);
      case 12 : while(time(NULL) <= end) { NL12++; }; dout << NL12; dout << "\n"; pthread_exit(NULL);
      case 13 : while(time(NULL) <= end) { NL13++; }; dout << NL13; dout << "\n"; pthread_exit(NULL);
      case 14 : while(time(NULL) <= end) { NL14++; }; dout << NL14; dout << "\n"; pthread_exit(NULL);
      case 15 : while(time(NULL) <= end) { NL15++; }; dout << NL15; dout << "\n"; pthread_exit(NULL);
      case 16 : while(time(NULL) <= end) { NL16++; }; dout << NL16; dout << "\n"; pthread_exit(NULL);
      case 17 : while(time(NULL) <= end) { NL17++; }; dout << NL17; dout << "\n"; pthread_exit(NULL);
      case 18 : while(time(NULL) <= end) { NL18++; }; dout << NL18; dout << "\n"; pthread_exit(NULL);
      case 19 : while(time(NULL) <= end) { NL19++; }; dout << NL19; dout << "\n"; pthread_exit(NULL);
      case 20 : while(time(NULL) <= end) { NL20++; }; dout << NL20; dout << "\n"; pthread_exit(NULL);
    }
    dout.close();
    string filename = "NLC.DAT";
	//cout << "Specify file name" << endl;
	//cin >> filename;
	din.open(filename.c_str());
    din >> NL1;
    cout << "Count: " << NL1 << endl;
    NUMTHREADS++;
    while (din) {
    	NUMLOOPS = NUMLOOPS + NL1;
    	NUMTHREADS++;
    	din >> NL1;
    	cout << "Value from file:" << NL1 << endl;
    }
    din.clear();
    din.close();
    
    
    //cout << NL0 << endl << NL1 << endl << NL2 << endl << NL3 << endl;
 
	//NUMLOOPS = NL0 + NL1 + NL2 + NL3 + NL4 + NL5 + NL5 + NL6 + NL7 + NL8 + NL9 + NL10 + NL11 + NL12 + NL13 + NL14 + NL15 + NL16 + NL17 + NL18 + NL19 + NL20;

    cout << "Number of jobs: " << NUMTHREADS << endl;
    cout << "Number of loops: " << NUMLOOPS << endl;
    //CountLoop(NUMTHREADS, NUMLOOPS);
    pthread_exit(NULL);
}
